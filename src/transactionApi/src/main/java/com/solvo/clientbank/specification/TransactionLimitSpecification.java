package com.solvo.clientbank.specification;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class TransactionLimitSpecification<T> {
    public Specification<T> findByAccountSpecification(Integer id, String field) {
        return (root, query, cb) -> cb.and(cb.equal(root.get(field), id));
    }
}
