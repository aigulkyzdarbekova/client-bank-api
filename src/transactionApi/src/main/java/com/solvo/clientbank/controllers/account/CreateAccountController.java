package com.solvo.clientbank.controllers.account;

import com.solvo.clientbank.dto.AccountDTO;
import com.solvo.clientbank.mapper.AccountMapper;
import com.solvo.clientbank.services.interfaces.AccountService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/accounts")
public class CreateAccountController {
    private final AccountService service;
    private final AccountMapper mapper;

    public CreateAccountController(AccountService service, AccountMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PostMapping
    public AccountDTO create(@RequestBody AccountDTO accountDTO) {
        return mapper.toDTO(service.create(mapper.toEntity(accountDTO)));
    }
}
