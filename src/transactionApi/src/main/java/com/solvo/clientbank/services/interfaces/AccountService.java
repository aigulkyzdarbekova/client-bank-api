package com.solvo.clientbank.services.interfaces;

import com.solvo.clientbank.models.Account;


public interface AccountService {
    Account read(Integer number);
    Account create(Account account);
}
