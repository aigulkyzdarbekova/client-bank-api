package com.solvo.clientbank.specification;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class BankTransactionByLimitSpecification<T> {
    public Specification<T> findByCategorySpecification(Integer accountFromId, Integer expenseCategoryId) {
        return (root, query, cb) -> cb.and(cb.equal(root.get("accountFrom"), accountFromId), cb.equal(root.get("expenseCategory"), expenseCategoryId), cb.isTrue(root.get("limitExceeded").as(Boolean.class)));
    }
}
