package com.solvo.clientbank.services.interfaces;

import com.solvo.clientbank.models.CurrencyCategory;


public interface CurrencyCategoryService {
    CurrencyCategory create(CurrencyCategory currencyCategory);
    CurrencyCategory read(String title);
}
