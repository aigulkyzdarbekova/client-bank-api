package com.solvo.clientbank.controllers.transactionlimit;

import com.solvo.clientbank.dto.TransactionLimitDTO;
import com.solvo.clientbank.mapper.TransactionLimitMapper;
import com.solvo.clientbank.services.interfaces.TransactionLimitService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/transaction_limits")
public class CreateTransactionLimitController {
    private final TransactionLimitService service;
    private final TransactionLimitMapper mapper;

    public CreateTransactionLimitController(TransactionLimitService service, TransactionLimitMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PostMapping
    public TransactionLimitDTO create(@RequestBody TransactionLimitDTO transactionLimitDTO) {
        return mapper.toDTO(service.create(mapper.toEntity(transactionLimitDTO)));
    }
}
