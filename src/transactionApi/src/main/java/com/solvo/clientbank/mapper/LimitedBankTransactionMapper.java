package com.solvo.clientbank.mapper;

import com.solvo.clientbank.dto.LimitedBankTransactionDTO;
import com.solvo.clientbank.interfaces.AbstractMapper;
import com.solvo.clientbank.models.Account;
import com.solvo.clientbank.models.BankTransaction;
import com.solvo.clientbank.models.CurrencyCategory;
import com.solvo.clientbank.models.ExpenseCategory;
import com.solvo.clientbank.services.interfaces.*;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class LimitedBankTransactionMapper extends AbstractMapper<BankTransaction, LimitedBankTransactionDTO> {
    private final ModelMapper mapper;
    private final CurrencyCategoryService currencyCategoryService;
    private final ExpenseCategoryService expenseCategoryService;
    private final AccountService accountService;
    private final TransactionLimitService transactionLimitService;
    private final BankTransactionService bankTransactionService;

    public LimitedBankTransactionMapper(ModelMapper mapper, CurrencyCategoryService currencyCategoryService, ExpenseCategoryService expenseCategoryService, AccountService accountService, TransactionLimitService transactionLimitService, BankTransactionService bankTransactionService) {
        super(BankTransaction.class, LimitedBankTransactionDTO.class);
        this.mapper = mapper;
        this.currencyCategoryService = currencyCategoryService;
        this.expenseCategoryService = expenseCategoryService;
        this.accountService = accountService;
        this.transactionLimitService = transactionLimitService;
        this.bankTransactionService = bankTransactionService;
    }


    @PostConstruct
    public void setupMapper(){
        mapper
                .createTypeMap(BankTransaction.class, LimitedBankTransactionDTO.class)
                .addMappings(mapper -> {
                    mapper.skip(LimitedBankTransactionDTO::setAccountFrom);
                    mapper.skip(LimitedBankTransactionDTO::setAccountTo);
                    mapper.skip(LimitedBankTransactionDTO::setCurrencyCategory);
                    mapper.skip(LimitedBankTransactionDTO::setExpenseCategory);
//                    mapper.skip(LimitedBankTransactionDTO::setLimitSum);
//                    mapper.skip(LimitedBankTransactionDTO::setLimitDateTime);
//                    mapper.skip(LimitedBankTransactionDTO::setLimitCurrencyShortName);

                })
                .setPostConverter(toDTOConverter());
        mapper
                .createTypeMap(LimitedBankTransactionDTO.class, BankTransaction.class)
                .addMappings(mapper -> mapper.skip(BankTransaction::setAccountFrom))
                .addMappings(mapper -> mapper.skip(BankTransaction::setAccountTo))
                .addMappings(mapper -> mapper.skip(BankTransaction::setCurrencyCategory))
                .addMappings(mapper -> mapper.skip(BankTransaction::setExpenseCategory))
                .setPostConverter(toEntityConverter());
    }


    @Override
    public void mapSpecificFields(BankTransaction source, LimitedBankTransactionDTO destination){
        destination.setAccountFrom(source.getAccountFrom().getNumber());
        destination.setAccountTo(source.getAccountTo().getNumber());
        destination.setCurrencyCategory(source.getCurrencyCategory().getTitle());
        destination.setExpenseCategory(source.getExpenseCategory().getTitle());
        destination.setLimitSum(source.getTransactionLimit().getInitialLimitAmount());
        destination.setLimitDateTime(source.getTransactionLimit().getCreated());
        destination.setLimitCurrencyShortName(source.getTransactionLimit().getCurrencyCategory().getTitle());
    }

    @Override
    public void mapSpecificFields(LimitedBankTransactionDTO source, BankTransaction destination){
        Integer accountNumberFrom = getAccountNumberFrom(source);
        Account accountFrom = accountService.read(accountNumberFrom);

        destination.setAccountFrom(accountFrom);


        Integer accountNumberTo = getAccountNumberTo(source);
        Account accountTo = accountService.read(accountNumberTo);

        destination.setAccountTo(accountTo);


        String currencyCategoryTitle = getCurrencyCategory(source);
        CurrencyCategory currencyCategory = currencyCategoryService.read(currencyCategoryTitle);

        destination.setCurrencyCategory(currencyCategory);


        String expenseCategoryTitle = getExpenseCategory(source);
        ExpenseCategory expenseCategory = expenseCategoryService.read(expenseCategoryTitle);

        destination.setExpenseCategory(expenseCategory);

    }


    private String getCurrencyCategory (LimitedBankTransactionDTO transactionDTO){
        return Objects.isNull(transactionDTO)
                ? null
                :transactionDTO
                .getCurrencyCategory();
    }

    private String getExpenseCategory (LimitedBankTransactionDTO transactionDTO){
        return Objects.isNull(transactionDTO)
                ? null
                :transactionDTO
                .getExpenseCategory();
    }

    private Integer getAccountNumberFrom (LimitedBankTransactionDTO LimitedBankTransactionDTO){
        return Objects.isNull(LimitedBankTransactionDTO)
                ? null
                :LimitedBankTransactionDTO
                .getAccountFrom();
    }


    private Integer getAccountNumberTo (LimitedBankTransactionDTO LimitedBankTransactionDTO){
        return Objects.isNull(LimitedBankTransactionDTO)
                ? null
                :LimitedBankTransactionDTO
                .getAccountTo();
    }

}
