package com.solvo.clientbank.models;

import com.solvo.clientbank.interfaces.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Table(name = "transaction_limits")
public class TransactionLimit extends AbstractEntity {

    private double initialLimitAmount;

    private double limitBalance;

    private double limitSum;

    @ManyToOne
    @JoinColumn(name = "expense_id")
    private ExpenseCategory expenseCategory;

    @ManyToOne
    @JoinColumn(name = "currency_id")
    private CurrencyCategory currencyCategory;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;
}
