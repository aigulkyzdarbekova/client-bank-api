package com.solvo.clientbank.mapper;

import com.solvo.clientbank.interfaces.AbstractMapper;
import com.solvo.clientbank.dto.CurrencyCategoryDTO;
import com.solvo.clientbank.models.CurrencyCategory;
import org.springframework.stereotype.Component;

@Component
public class CurrencyCategoryMapper extends AbstractMapper<CurrencyCategory, CurrencyCategoryDTO> {
    public CurrencyCategoryMapper() {
        super(CurrencyCategory.class, CurrencyCategoryDTO.class);
    }
}