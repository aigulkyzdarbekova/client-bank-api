package com.solvo.clientbank.services;

import com.solvo.clientbank.exceptions.TransactionLimitNotFoundException;
import com.solvo.clientbank.models.TransactionLimit;
import com.solvo.clientbank.repositories.TransactionLimitRepository;
import com.solvo.clientbank.services.interfaces.AccountService;
import com.solvo.clientbank.services.interfaces.TransactionLimitService;
import com.solvo.clientbank.specification.TransactionLimitSpecification;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class TransactionLimitServiceImpl implements TransactionLimitService {
    private final TransactionLimitRepository transactionLimitRepository;
    private final TransactionLimitSpecification<TransactionLimit> transactionLimitSpecification;
    private final AccountService accountService;


    public TransactionLimitServiceImpl(TransactionLimitRepository transactionLimitRepository, TransactionLimitSpecification<TransactionLimit> transactionLimitSpecification, AccountService accountService) {
        this.transactionLimitRepository = transactionLimitRepository;
        this.transactionLimitSpecification = transactionLimitSpecification;
        this.accountService = accountService;
    }

    @Override
    public TransactionLimit create(TransactionLimit transactionLimit) {
        return transactionLimitRepository.save(transactionLimit);
    }



    private TransactionLimit getTransactionLimitOrThrowException(Optional<TransactionLimit> transactionLimit) {
        if (transactionLimit.isPresent()) {
            return transactionLimit.get();
        }

        throw new TransactionLimitNotFoundException();
    }

}
