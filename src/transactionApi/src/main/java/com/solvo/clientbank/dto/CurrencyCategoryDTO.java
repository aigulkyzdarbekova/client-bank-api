package com.solvo.clientbank.dto;


import com.solvo.clientbank.interfaces.AbstractDTO;
import lombok.*;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class CurrencyCategoryDTO extends AbstractDTO {

    private String title;

}
