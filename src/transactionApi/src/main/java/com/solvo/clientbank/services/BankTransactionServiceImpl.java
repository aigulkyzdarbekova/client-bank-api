package com.solvo.clientbank.services;

import com.solvo.clientbank.models.BankTransaction;
import com.solvo.clientbank.models.TransactionLimit;
import com.solvo.clientbank.repositories.BankTransactionRepository;
import com.solvo.clientbank.repositories.TransactionLimitRepository;
import com.solvo.clientbank.services.interfaces.AccountService;
import com.solvo.clientbank.services.interfaces.CurrencyRateService;
import com.solvo.clientbank.services.interfaces.ExpenseCategoryService;
import com.solvo.clientbank.services.interfaces.BankTransactionService;
import com.solvo.clientbank.specification.BankTransactionByLimitSpecification;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BankTransactionServiceImpl implements BankTransactionService {
    private final BankTransactionRepository transactionRepository;
    private final AccountService accountService;
    private final ExpenseCategoryService expenseCategoryService;
    private final TransactionLimitRepository transactionLimitRepository;
    private final BankTransactionByLimitSpecification<BankTransaction> specification;
    private final CurrencyRateService currencyRateService;

    public BankTransactionServiceImpl(BankTransactionRepository transactionRepository, AccountService accountService, ExpenseCategoryService expenseCategoryService, TransactionLimitRepository transactionLimitRepository, BankTransactionByLimitSpecification<BankTransaction> specification, CurrencyRateService currencyRateService) {
        this.transactionRepository = transactionRepository;
        this.accountService = accountService;
        this.expenseCategoryService = expenseCategoryService;
        this.transactionLimitRepository = transactionLimitRepository;
        this.specification = specification;
        this.currencyRateService = currencyRateService;
    }

    @Override
    public BankTransaction create(BankTransaction bankTransaction) {
        var account = accountService.read(bankTransaction.getAccountFrom().getNumber());
        var expenseCategory = expenseCategoryService.read(bankTransaction.getExpenseCategory().getTitle());
        var transactionLimitList  =  transactionLimitRepository.findTransactionLimitByExpenseCategory_IdAndAccount_Id(expenseCategory.getId(), account.getId());
        TransactionLimit transactionLimit = new TransactionLimit();
        if(transactionLimitList.size() == 0){
            transactionLimit.setAccount(account);
            transactionLimit.setExpenseCategory(expenseCategory);
            transactionLimit.setInitialLimitAmount(0);
            transactionLimit = transactionLimitRepository.save(transactionLimit);
        }else{
            transactionLimit = transactionLimitList.get(transactionLimitList.size()-1);
        }


        Double rate;

        if(currencyRateService.getCurrencyRate().getClose()!= null){
            rate = currencyRateService.getCurrencyRate().getClose();
        }else{
            rate = currencyRateService.getCurrencyRate().getPreviousClose();
        }

        transactionLimit.setLimitSum(transactionLimit.getLimitSum() + (bankTransaction.getTransactionSum()*rate));
        transactionLimit.setLimitBalance((transactionLimit.getInitialLimitAmount()) - (transactionLimit.getLimitSum()));

        if(transactionLimit.getLimitBalance() < 0){
            bankTransaction.setLimitExceeded(true);
        }


        bankTransaction.setTransactionLimit(transactionLimit);
        transactionLimitRepository.save(transactionLimit);

        return transactionRepository.save(bankTransaction);
    }



    @Override
    public List<BankTransaction> readAll(BankTransaction transaction) {
        var accountId = transaction.getAccountFrom().getId();
        var expenseId = transaction.getExpenseCategory().getId();

        return transactionRepository.findAll(Specification.where(specification.findByCategorySpecification(accountId, expenseId)));
    }

}
