package com.solvo.clientbank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class ExpenseCategoryNotFoundException extends RuntimeException{
    public ExpenseCategoryNotFoundException(String message) {
        super(message);
    }

    public ExpenseCategoryNotFoundException() {
        super("Expense category not found in database");
    }

}
