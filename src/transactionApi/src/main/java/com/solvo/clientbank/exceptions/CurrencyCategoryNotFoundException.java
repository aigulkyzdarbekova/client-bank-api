package com.solvo.clientbank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class CurrencyCategoryNotFoundException extends RuntimeException{
    public CurrencyCategoryNotFoundException(String message) {
        super(message);
    }

    public CurrencyCategoryNotFoundException() {
        super("Currency category not found in database");
    }

}
