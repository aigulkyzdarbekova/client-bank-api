package com.solvo.clientbank.mapper;

import com.solvo.clientbank.dto.TransactionLimitDTO;
import com.solvo.clientbank.interfaces.AbstractMapper;
import com.solvo.clientbank.models.Account;
import com.solvo.clientbank.models.CurrencyCategory;
import com.solvo.clientbank.models.ExpenseCategory;
import com.solvo.clientbank.models.TransactionLimit;
import com.solvo.clientbank.services.interfaces.AccountService;
import com.solvo.clientbank.services.interfaces.CurrencyCategoryService;
import com.solvo.clientbank.services.interfaces.ExpenseCategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class TransactionLimitMapper extends AbstractMapper<TransactionLimit, TransactionLimitDTO> {
    private final ModelMapper mapper;
    private final ExpenseCategoryService expenseCategoryService;
    private final CurrencyCategoryService currencyCategoryService;
    private final AccountService accountService;

    public TransactionLimitMapper(ModelMapper mapper, ExpenseCategoryService expenseCategoryService, CurrencyCategoryService currencyCategoryService, AccountService accountService) {
        super(TransactionLimit.class, TransactionLimitDTO.class);
        this.mapper = mapper;
        this.expenseCategoryService = expenseCategoryService;
        this.currencyCategoryService = currencyCategoryService;
        this.accountService = accountService;
    }


    @PostConstruct
    public void setupMapper(){
        mapper
                .createTypeMap(TransactionLimit.class, TransactionLimitDTO.class)
                .addMappings(mapper -> {
                    mapper.skip(TransactionLimitDTO::setExpenseCategory);
                    mapper.skip(TransactionLimitDTO::setCurrencyCategory);
                    mapper.skip(TransactionLimitDTO::setAccount);
                })
                .setPostConverter(toDTOConverter());
        mapper
                .createTypeMap(TransactionLimitDTO.class, TransactionLimit.class)
                .addMappings(mapper -> mapper.skip(TransactionLimit::setExpenseCategory))
                .addMappings(mapper -> mapper.skip(TransactionLimit::setCurrencyCategory))
                .addMappings(mapper -> mapper.skip(TransactionLimit::setAccount))
                .setPostConverter(toEntityConverter());
    }


    @Override
    public void mapSpecificFields(TransactionLimit source, TransactionLimitDTO destination){
        destination.setExpenseCategory(source.getExpenseCategory().getTitle());
        destination.setCurrencyCategory(source.getCurrencyCategory().getTitle());
        destination.setAccount(source.getAccount().getNumber());
    }

    @Override
    public void mapSpecificFields(TransactionLimitDTO source, TransactionLimit destination){
        String currencyCategoryTitle = getCurrencyCategory(source);
        CurrencyCategory currencyCategory = currencyCategoryService.read(currencyCategoryTitle);

        destination.setCurrencyCategory(currencyCategory);


        String expenseCategoryTitle = getExpenseCategory(source);
        ExpenseCategory expenseCategory = expenseCategoryService.read(expenseCategoryTitle);

        destination.setExpenseCategory(expenseCategory);


        Integer accountNumber = getAccountNumber(source);
        Account account = accountService.read(accountNumber);

        destination.setAccount(account);
    }


    private String getCurrencyCategory (TransactionLimitDTO transactionLimitDTO){
        return Objects.isNull(transactionLimitDTO)
                ? null
                :transactionLimitDTO
                .getCurrencyCategory();
    }


    private String getExpenseCategory (TransactionLimitDTO transactionLimitDTO){
        return Objects.isNull(transactionLimitDTO)
                ? null
                :transactionLimitDTO
                .getExpenseCategory();
    }


    private Integer getAccountNumber (TransactionLimitDTO transactionLimitDTO){
        return Objects.isNull(transactionLimitDTO)
                ? null
                :transactionLimitDTO
                .getAccount();
    }

}
