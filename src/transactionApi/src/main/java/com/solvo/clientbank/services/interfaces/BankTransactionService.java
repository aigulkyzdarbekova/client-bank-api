package com.solvo.clientbank.services.interfaces;
import com.solvo.clientbank.models.BankTransaction;

import java.util.List;


public interface BankTransactionService {
    BankTransaction create(BankTransaction transaction);
    List<BankTransaction> readAll(BankTransaction transaction);

}
