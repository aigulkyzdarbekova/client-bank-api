package com.solvo.clientbank.services;

import com.solvo.clientbank.dto.TransactionLimitDTO;
import com.solvo.clientbank.exceptions.AccountNotFoundException;
import com.solvo.clientbank.mapper.TransactionLimitMapper;
import com.solvo.clientbank.models.Account;
import com.solvo.clientbank.models.ExpenseCategory;
import com.solvo.clientbank.models.TransactionLimit;
import com.solvo.clientbank.repositories.AccountRepository;
import com.solvo.clientbank.repositories.TransactionLimitRepository;
import com.solvo.clientbank.services.interfaces.AccountService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService{
    private final AccountRepository accountRepository;
    private final TransactionLimitRepository repository;


    public AccountServiceImpl(AccountRepository accountRepository, TransactionLimitRepository service) {
        this.accountRepository = accountRepository;
        this.repository = service;

    }


    @Override
    public Account read(Integer numberAccount) {
        Optional<Account> optionalAccount = accountRepository.findByNumber(numberAccount);

        if(optionalAccount.isEmpty()){
            Account account = new Account();
            account.setNumber(numberAccount);
            var newAccount = accountRepository.save(account);

            ExpenseCategory expenseCategory = new ExpenseCategory();
            expenseCategory.setTitle("услуги");

            TransactionLimit transactionLimit = new TransactionLimit();
            transactionLimit.setAccount(newAccount);
            transactionLimit.setExpenseCategory(expenseCategory);
            repository.save(transactionLimit);


            ExpenseCategory expenseCategory2 = new ExpenseCategory();
            expenseCategory2.setTitle("товары");

            TransactionLimit transactionLimit2 = new TransactionLimit();
            transactionLimit2.setAccount(newAccount);
            transactionLimit2.setExpenseCategory(expenseCategory2);
            repository.save(transactionLimit2);

            return newAccount;


        }

        return optionalAccount.get();
    }


    @Override
    public Account create(Account account) {
        Optional<Account> optionalAccount = accountRepository.findByNumber(account.getNumber());
        if(optionalAccount.isEmpty()){
            return accountRepository.save(account);
        }
        return optionalAccount.get();
    }


    private Account getAccountOrThrowException(Optional<Account> account) {
        if (account.isPresent()) {
            return account.get();
        }

        throw new AccountNotFoundException();
    }
}
