package com.solvo.clientbank.controllers.banktransaction;

import com.solvo.clientbank.dto.LimitedBankTransactionDTO;
import com.solvo.clientbank.mapper.LimitedBankTransactionMapper;
import com.solvo.clientbank.services.interfaces.BankTransactionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/bank_transactions")
public class ReadAllLimitedBankTransactionController {
    private final BankTransactionService service;
    private final LimitedBankTransactionMapper mapper;

    public ReadAllLimitedBankTransactionController(BankTransactionService service, LimitedBankTransactionMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PostMapping
    public List<LimitedBankTransactionDTO> readAll(@RequestBody LimitedBankTransactionDTO transactionDTO) {
        return mapper.toDTOList(service.readAll(mapper.toEntity(transactionDTO)));
    }
}
