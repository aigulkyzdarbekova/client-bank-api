package com.solvo.clientbank.repositories;
import com.solvo.clientbank.models.BankTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankTransactionRepository extends JpaRepository<BankTransaction, Integer>, JpaSpecificationExecutor {
    List<BankTransaction> readAllByAccountFrom_IdAndExpenseCategory_IdAndCurrencyCategory_Id(Integer accountFrom_id, Integer expenseCategory_id, Integer currencyCategory_id);

}
