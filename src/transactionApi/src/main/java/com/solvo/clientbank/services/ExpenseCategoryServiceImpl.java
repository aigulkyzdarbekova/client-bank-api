package com.solvo.clientbank.services;
import com.solvo.clientbank.exceptions.CurrencyCategoryNotFoundException;
import com.solvo.clientbank.exceptions.ExpenseCategoryNotFoundException;
import com.solvo.clientbank.models.CurrencyCategory;
import com.solvo.clientbank.models.ExpenseCategory;
import com.solvo.clientbank.repositories.ExpenseCategoryRepository;
import com.solvo.clientbank.services.interfaces.ExpenseCategoryService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ExpenseCategoryServiceImpl implements ExpenseCategoryService {
    private final ExpenseCategoryRepository expenseCategoryRepository;

    public ExpenseCategoryServiceImpl(ExpenseCategoryRepository expenseCategoryRepository) {
        this.expenseCategoryRepository = expenseCategoryRepository;
    }

    @Override
    public ExpenseCategory create(ExpenseCategory expenseCategory) {
        return expenseCategoryRepository.save(expenseCategory);
    }

    @Override
    public ExpenseCategory read(String title) {
        Optional<ExpenseCategory> optionalExpenseCategory = expenseCategoryRepository.findByTitle(title);
        return getExpenseCategoryOrThrowException(optionalExpenseCategory);
    }


    private ExpenseCategory getExpenseCategoryOrThrowException(Optional<ExpenseCategory> expenseCategory) {
        if (expenseCategory.isPresent()) {
            return expenseCategory.get();
        }

        throw new ExpenseCategoryNotFoundException();
    }
}
