package com.solvo.clientbank.controllers.expensecategory;

import com.solvo.clientbank.dto.ExpenseCategoryDTO;
import com.solvo.clientbank.mapper.ExpenseCategoryMapper;
import com.solvo.clientbank.services.interfaces.ExpenseCategoryService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/expense_categories")
public class CreateExpenseCategoryController {
    private final ExpenseCategoryService service;
    private final ExpenseCategoryMapper mapper;

    public CreateExpenseCategoryController(ExpenseCategoryService service, ExpenseCategoryMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PostMapping
    public ExpenseCategoryDTO create(@RequestBody ExpenseCategoryDTO expenseCategoryDTO) {
        return mapper.toDTO(service.create(mapper.toEntity(expenseCategoryDTO)));
    }
}
