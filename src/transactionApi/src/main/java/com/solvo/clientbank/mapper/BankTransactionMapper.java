package com.solvo.clientbank.mapper;

import com.solvo.clientbank.dto.LimitedBankTransactionDTO;
import com.solvo.clientbank.interfaces.AbstractMapper;
import com.solvo.clientbank.dto.BankTransactionDTO;
import com.solvo.clientbank.models.Account;
import com.solvo.clientbank.models.CurrencyCategory;
import com.solvo.clientbank.models.ExpenseCategory;
import com.solvo.clientbank.models.BankTransaction;
import com.solvo.clientbank.services.interfaces.*;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class BankTransactionMapper extends AbstractMapper<BankTransaction, BankTransactionDTO> {
    private final ModelMapper mapper;
    private final CurrencyCategoryService currencyCategoryService;
    private final ExpenseCategoryService expenseCategoryService;
    private final AccountService accountService;
    private final TransactionLimitService transactionLimitService;

    public BankTransactionMapper(ModelMapper mapper, CurrencyCategoryService currencyCategoryService, ExpenseCategoryService expenseCategoryService, AccountService accountService, TransactionLimitService transactionLimitService) {
        super(BankTransaction.class, BankTransactionDTO.class);
        this.mapper = mapper;
        this.currencyCategoryService = currencyCategoryService;
        this.expenseCategoryService = expenseCategoryService;
        this.accountService = accountService;
        this.transactionLimitService = transactionLimitService;
    }


    @PostConstruct
    public void setupMapper(){
        mapper
                .createTypeMap(BankTransaction.class, BankTransactionDTO.class)
                .addMappings(mapper -> {
                    mapper.skip(BankTransactionDTO::setAccountFrom);
                    mapper.skip(BankTransactionDTO::setAccountTo);
                    mapper.skip(BankTransactionDTO::setCurrencyCategory);
                    mapper.skip(BankTransactionDTO::setExpenseCategory);
                })
                .setPostConverter(toDTOConverter());
        mapper
                .createTypeMap(BankTransactionDTO.class, BankTransaction.class)
                .addMappings(mapper -> mapper.skip(BankTransaction::setAccountFrom))
                .addMappings(mapper -> mapper.skip(BankTransaction::setAccountTo))
                .addMappings(mapper -> mapper.skip(BankTransaction::setCurrencyCategory))
                .addMappings(mapper -> mapper.skip(BankTransaction::setExpenseCategory))
                .setPostConverter(toEntityConverter());
    }


    @Override
    public void mapSpecificFields(BankTransaction source, BankTransactionDTO destination){
        destination.setAccountFrom(source.getAccountFrom().getNumber());
        destination.setAccountTo(source.getAccountTo().getNumber());
        destination.setCurrencyCategory(source.getCurrencyCategory().getTitle());
        destination.setExpenseCategory(source.getExpenseCategory().getTitle());
    }

    @Override
    public void mapSpecificFields(BankTransactionDTO source, BankTransaction destination){
        String currencyCategoryTitle = getCurrencyCategory(source);
        CurrencyCategory currencyCategory = currencyCategoryService.read(currencyCategoryTitle);

        destination.setCurrencyCategory(currencyCategory);


        String expenseCategoryTitle = getExpenseCategory(source);
        ExpenseCategory expenseCategory = expenseCategoryService.read(expenseCategoryTitle);

        destination.setExpenseCategory(expenseCategory);


        Integer accountNumberFrom = getAccountNumberFrom(source);
        Account accountFrom = accountService.read(accountNumberFrom);

        destination.setAccountFrom(accountFrom);


        Integer accountNumberTo = getAccountNumberTo(source);
        Account accountTo = accountService.read(accountNumberTo);

        destination.setAccountTo(accountTo);
    }


    private String getCurrencyCategory (BankTransactionDTO transactionDTO){
        return Objects.isNull(transactionDTO)
                ? null
                :transactionDTO
                .getCurrencyCategory();
    }

    private String getExpenseCategory (BankTransactionDTO transactionDTO){
        return Objects.isNull(transactionDTO)
                ? null
                :transactionDTO
                .getExpenseCategory();
    }

    private Integer getAccountNumberFrom (BankTransactionDTO bankTransactionDTO){
        return Objects.isNull(bankTransactionDTO)
                ? null
                :bankTransactionDTO
                .getAccountFrom();
    }


    private Integer getAccountNumberTo (BankTransactionDTO bankTransactionDTO){
        return Objects.isNull(bankTransactionDTO)
                ? null
                :bankTransactionDTO
                .getAccountTo();
    }

}
