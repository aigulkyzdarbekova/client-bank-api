package com.solvo.clientbank.services.interfaces;
import com.solvo.clientbank.models.ExpenseCategory;

public interface ExpenseCategoryService {
    ExpenseCategory create(ExpenseCategory expenseCategory);
    ExpenseCategory read(String title);
}
