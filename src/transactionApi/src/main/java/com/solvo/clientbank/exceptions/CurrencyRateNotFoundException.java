package com.solvo.clientbank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class CurrencyRateNotFoundException extends RuntimeException{
    public CurrencyRateNotFoundException(String message) {
        super(message);
    }

    public CurrencyRateNotFoundException() {
        super("Currency rate not found in database");
    }

}
