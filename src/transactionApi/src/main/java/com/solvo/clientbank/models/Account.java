package com.solvo.clientbank.models;

import com.solvo.clientbank.interfaces.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Table(name = "accounts")
public class Account extends AbstractEntity {

    private Integer number;

}
