package com.solvo.clientbank.services;

import com.solvo.clientbank.exceptions.AccountNotFoundException;
import com.solvo.clientbank.exceptions.CurrencyRateNotFoundException;
import com.solvo.clientbank.models.Account;
import com.solvo.clientbank.models.CurrencyRate;
import com.solvo.clientbank.repositories.CurrencyRateRepository;
import com.solvo.clientbank.services.interfaces.CurrencyRateService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Optional;


@Service
public class CurrencyRateServiceImpl implements CurrencyRateService {
    private final CurrencyRateRepository repository;
    private final RestTemplate restTemplate;

    public CurrencyRateServiceImpl(CurrencyRateRepository repository, RestTemplateBuilder restTemplateBuilder) {
        this.repository = repository;
        this.restTemplate = restTemplateBuilder.build();
    }

    public CurrencyRate getCurrencyRate() {
        String url = String.format("https://www.alphavantage.co/query?function=%s&from_symbol=%s&to_symbol=%s&apikey=%s", CurrencyRate.FUNCTION, CurrencyRate.FROM_SYMBOL, CurrencyRate.TO_SYMBOL, CurrencyRate.APIKEY);

        String result = this.restTemplate.getForObject(url, String.class);
        JSONObject jsonObject = new JSONObject(result);

        LocalDate today = LocalDate.now();
        LocalDate yesterday = LocalDate.now().minusDays(1);


        var daily = jsonObject.get("Time Series FX (Daily)").toString();
        System.out.println(daily);
        JSONObject dailySt = new JSONObject(daily);

        var last = "";
        var previous = "";

        CurrencyRate currencyRate = new CurrencyRate();

        try {
            last = dailySt.get(today.toString()).toString();
        }catch (JSONException e){
            System.out.println(today + " курс на сегодня не найден!");
        }

        try {
            previous = dailySt.get(yesterday.toString()).toString();
        }catch (JSONException ex){
            System.out.println(yesterday + " курс на предыдущий день не найден!");
        }


        if(last.length() > 0){

            var rate = new JSONObject(last);
            currencyRate.setClose(Double.parseDouble(rate.get("4. close").toString()));
            currencyRate.setOperationCloseDate(today);
            System.out.println("Today: " + rate.get("4. close").toString());

        }

        if (previous.length() > 0){

            var rate = new JSONObject(previous);
            currencyRate.setPreviousClose(Double.parseDouble(rate.get("4. close").toString()));
            currencyRate.setOperationPreviousDate(yesterday);
            System.out.println("Yesterday: " + rate.get("4. close").toString());
        }

        if(last.length() == 0 && previous.length() == 0){
            var rateLastOptional= repository.findTopByOrderByIdDesc();
            var rateLast = getCurrencyRateOrThrowException(rateLastOptional);

            currencyRate.setPreviousClose(rateLast.getPreviousClose());
            currencyRate.setOperationPreviousDate(rateLast.getOperationPreviousDate());
        }

        return repository.save(currencyRate);
    }



    private CurrencyRate getCurrencyRateOrThrowException(Optional<CurrencyRate> currencyRate) {
        if (currencyRate.isPresent()) {
            return currencyRate.get();
        }

        throw new CurrencyRateNotFoundException();
    }


}
