package com.solvo.clientbank.dto;
import com.solvo.clientbank.interfaces.AbstractDTO;
import lombok.*;



@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class AccountDTO extends AbstractDTO {

    private int number;

}
