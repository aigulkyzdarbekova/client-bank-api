package com.solvo.clientbank.dto;

import com.solvo.clientbank.interfaces.AbstractDTO;
import lombok.*;

import java.time.LocalDate;


@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class CurrencyRateDTO extends AbstractDTO {

    private LocalDate operationCloseDate;
    private Double close;

    private LocalDate operationPreviousDate;
    private Double previousClose;

}
