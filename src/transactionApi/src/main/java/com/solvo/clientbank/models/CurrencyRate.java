package com.solvo.clientbank.models;

import com.solvo.clientbank.interfaces.AbstractEntity;
import lombok.*;
import org.springframework.data.annotation.Transient;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;


@Entity
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Table(name = "currency_rates")
public class CurrencyRate extends AbstractEntity {

    @Transient
    public static final String FUNCTION = "FX_DAILY";
    @Transient
    public static final String FROM_SYMBOL = "KZT";
    @Transient
    public static final String TO_SYMBOL = "USD";
    @Transient
    public static final String APIKEY = "H43VUIPUR68X9T5G";

    private LocalDate operationCloseDate;
    private Double close = null;

    private LocalDate operationPreviousDate;
    private Double previousClose = null;
}
