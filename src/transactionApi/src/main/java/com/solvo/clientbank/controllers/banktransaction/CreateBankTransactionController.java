package com.solvo.clientbank.controllers.banktransaction;

import com.solvo.clientbank.dto.BankTransactionDTO;
import com.solvo.clientbank.mapper.BankTransactionMapper;
import com.solvo.clientbank.services.interfaces.BankTransactionService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/transactions")
public class CreateBankTransactionController {
    private final BankTransactionService service;
    private final BankTransactionMapper mapper;

    public CreateBankTransactionController(BankTransactionService service, BankTransactionMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PostMapping
    public BankTransactionDTO create(@RequestBody BankTransactionDTO transactionDTO) {
        return mapper.toDTO(service.create(mapper.toEntity(transactionDTO)));
    }
}
