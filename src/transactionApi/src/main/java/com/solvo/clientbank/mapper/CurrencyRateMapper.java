package com.solvo.clientbank.mapper;

import com.solvo.clientbank.dto.CurrencyRateDTO;
import com.solvo.clientbank.interfaces.AbstractMapper;
import com.solvo.clientbank.models.CurrencyRate;
import org.springframework.stereotype.Component;

@Component
public class CurrencyRateMapper extends AbstractMapper<CurrencyRate, CurrencyRateDTO> {
    public CurrencyRateMapper() {
        super(CurrencyRate.class, CurrencyRateDTO.class);
    }
}