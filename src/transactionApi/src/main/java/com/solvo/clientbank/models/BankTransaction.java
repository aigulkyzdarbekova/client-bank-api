package com.solvo.clientbank.models;

import com.solvo.clientbank.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Table(name = "bank_transactions")
public class BankTransaction extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "account_from_id")
    private Account accountFrom;

    @ManyToOne
    @JoinColumn(name = "account_to_id")
    private Account accountTo;

    private LocalDateTime transactionDate;

    private double transactionSum;

    private boolean limitExceeded;

    @ManyToOne
    @JoinColumn(name = "currency_id")
    private CurrencyCategory currencyCategory;

    @ManyToOne
    @JoinColumn(name = "expense_id")
    private ExpenseCategory expenseCategory;

    @ManyToOne
    @JoinColumn(name = "transaction_limit_id")
    private TransactionLimit transactionLimit;

}
