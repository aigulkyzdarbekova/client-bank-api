package com.solvo.clientbank.mapper;

import com.solvo.clientbank.interfaces.AbstractMapper;
import com.solvo.clientbank.dto.ExpenseCategoryDTO;
import com.solvo.clientbank.models.ExpenseCategory;
import org.springframework.stereotype.Component;

@Component
public class ExpenseCategoryMapper extends AbstractMapper<ExpenseCategory, ExpenseCategoryDTO> {
    public ExpenseCategoryMapper() {
        super(ExpenseCategory.class, ExpenseCategoryDTO.class);
    }
}