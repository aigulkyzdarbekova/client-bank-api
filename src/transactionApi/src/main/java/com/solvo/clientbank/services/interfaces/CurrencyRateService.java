package com.solvo.clientbank.services.interfaces;

import com.solvo.clientbank.models.CurrencyRate;


public interface CurrencyRateService {
    CurrencyRate getCurrencyRate();
}
