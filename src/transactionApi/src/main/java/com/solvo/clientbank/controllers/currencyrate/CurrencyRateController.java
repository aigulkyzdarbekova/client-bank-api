package com.solvo.clientbank.controllers.currencyrate;

import com.solvo.clientbank.dto.CurrencyRateDTO;
import com.solvo.clientbank.mapper.CurrencyRateMapper;
import com.solvo.clientbank.services.interfaces.CurrencyRateService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/currency_rates")
public class CurrencyRateController {
    private final CurrencyRateService service;
    private final CurrencyRateMapper mapper;

    public CurrencyRateController(CurrencyRateService service, CurrencyRateMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping()
    public CurrencyRateDTO readRate(){
        return mapper.toDTO(service.getCurrencyRate());
    }
}
