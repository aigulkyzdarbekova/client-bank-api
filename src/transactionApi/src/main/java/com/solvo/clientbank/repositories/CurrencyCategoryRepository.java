package com.solvo.clientbank.repositories;
import com.solvo.clientbank.models.CurrencyCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CurrencyCategoryRepository extends JpaRepository<CurrencyCategory, Integer>, JpaSpecificationExecutor<CurrencyCategory> {
    Optional<CurrencyCategory> findByTitle(String title);
}
