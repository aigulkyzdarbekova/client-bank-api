package com.solvo.clientbank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class TransactionLimitNotFoundException extends RuntimeException{
    public TransactionLimitNotFoundException(String message) {
        super(message);
    }

    public TransactionLimitNotFoundException() {
        super("Transaction limit not found in database");
    }

}
