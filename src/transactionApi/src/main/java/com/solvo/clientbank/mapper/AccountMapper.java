package com.solvo.clientbank.mapper;

import com.solvo.clientbank.dto.AccountDTO;
import com.solvo.clientbank.interfaces.AbstractMapper;
import com.solvo.clientbank.models.Account;
import org.springframework.stereotype.Component;


@Component
public class AccountMapper extends AbstractMapper<Account, AccountDTO> {
    public AccountMapper() {
        super(Account.class, AccountDTO.class);
    }
}
