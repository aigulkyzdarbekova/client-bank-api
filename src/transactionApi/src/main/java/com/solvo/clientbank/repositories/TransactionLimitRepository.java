package com.solvo.clientbank.repositories;
import com.solvo.clientbank.models.TransactionLimit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionLimitRepository extends JpaRepository<TransactionLimit, Integer>, JpaSpecificationExecutor<TransactionLimit> {
    List<TransactionLimit> findTransactionLimitByExpenseCategory_IdAndAccount_Id(Integer expenseCategoryId, Integer accountNumberId);
    List<TransactionLimit> findTransactionLimitByAccount_Id(Integer accountNumberId);
}
