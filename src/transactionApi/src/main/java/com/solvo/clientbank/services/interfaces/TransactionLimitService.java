package com.solvo.clientbank.services.interfaces;
import com.solvo.clientbank.models.TransactionLimit;

import java.util.List;


public interface TransactionLimitService {
    TransactionLimit create(TransactionLimit transactionLimit);
}
