package com.solvo.clientbank.dto;
import com.solvo.clientbank.interfaces.AbstractDTO;
import lombok.*;



@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class TransactionLimitDTO extends AbstractDTO {

    private double initialLimitAmount;

    private String expenseCategory;
    private String currencyCategory;

    private Integer account;
}
