package com.solvo.clientbank.services;
import com.solvo.clientbank.exceptions.CurrencyCategoryNotFoundException;
import com.solvo.clientbank.models.CurrencyCategory;
import com.solvo.clientbank.repositories.CurrencyCategoryRepository;
import com.solvo.clientbank.services.interfaces.CurrencyCategoryService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CurrencyCategoryServiceImpl implements CurrencyCategoryService{
    private final CurrencyCategoryRepository currencyCategoryRepository;


    public CurrencyCategoryServiceImpl(CurrencyCategoryRepository currencyCategoryRepository) {
        this.currencyCategoryRepository = currencyCategoryRepository;
    }


    @Override
    public CurrencyCategory create(CurrencyCategory currencyCategory) {
        return currencyCategoryRepository.save(currencyCategory);
    }

    @Override
    public CurrencyCategory read(String title) {
        Optional<CurrencyCategory> optionalCurrencyCategory = currencyCategoryRepository.findByTitle(title);
        return getCurrencyCategoryOrThrowException(optionalCurrencyCategory);
    }


    private CurrencyCategory getCurrencyCategoryOrThrowException(Optional<CurrencyCategory> currencyCategory) {
        if (currencyCategory.isPresent()) {
            return currencyCategory.get();
        }

        throw new CurrencyCategoryNotFoundException();
    }
}
