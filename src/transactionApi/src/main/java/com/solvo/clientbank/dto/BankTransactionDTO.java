package com.solvo.clientbank.dto;
import com.solvo.clientbank.interfaces.AbstractDTO;
import lombok.*;

import java.time.LocalDateTime;


@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class BankTransactionDTO extends AbstractDTO {

    private Integer accountFrom;
    private Integer accountTo;

    private LocalDateTime transactionDate;

    private double transactionSum;

    private String currencyCategory;
    private String expenseCategory;

}
