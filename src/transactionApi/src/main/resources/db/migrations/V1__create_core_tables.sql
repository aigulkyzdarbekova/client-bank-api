--Модуль виды валют.

CREATE TABLE currency_categories
(
    "id"          	    SERIAL       NOT NULL,
    "created"     	    TIMESTAMP    NOT NULL,
    "updated"     	    TIMESTAMP    DEFAULT NULL,
    "title"   	        VARCHAR      NOT NULL,

    CONSTRAINT currency_categories_pkey
        PRIMARY KEY(id)
);


--Модуль виды операции.

CREATE TABLE expense_categories
(
    "id"          	    SERIAL       NOT NULL,
    "created"     	    TIMESTAMP    NOT NULL,
    "updated"     	    TIMESTAMP    DEFAULT NULL,
    "title"   	        VARCHAR      NOT NULL,

    CONSTRAINT expense_categories_pkey
    PRIMARY KEY(id)
);


--Модуль счета пользователей.

CREATE TABLE accounts
(
    "id"          	    SERIAL       NOT NULL,
    "created"     	    TIMESTAMP    NOT NULL,
    "updated"     	    TIMESTAMP    DEFAULT NULL,
    "number"   	        INT          NOT NULL,

    CONSTRAINT accounts_pkey
        PRIMARY KEY(id)
);



--Модуль лимиты по транзакциям

CREATE TABLE transaction_limits
(
    "id"          	        SERIAL       NOT NULL,
    "created"     	        TIMESTAMP    NOT NULL,
    "updated"     	        TIMESTAMP    DEFAULT NULL,
    "initialLimitAmount"   	FLOAT        DEFAULT 0,
    "limitBalance"          FLOAT        DEFAULT 0,
    "limitSum"              FLOAT        DEFAULT 0,
    "expense_id"            INT,
    "currency_id"           INT          DEFAULT 1,
    "account_id"            INT,


    CONSTRAINT transaction_limits_pkey
        PRIMARY KEY(id),


    CONSTRAINT fk_transaction_limit_expense_category
        FOREIGN KEY (expense_id)
            REFERENCES expense_categories (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,


    CONSTRAINT fk_transaction_limit_currency_category
        FOREIGN KEY (currency_id)
            REFERENCES currency_categories (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,


    CONSTRAINT fk_transaction_limit_account
        FOREIGN KEY (account_id)
            REFERENCES accounts (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);



CREATE TABLE bank_transactions
(
    "id"                    SERIAL       NOT NULL,
    "created"               TIMESTAMP    NOT NULL,
    "updated"               TIMESTAMP    DEFAULT NULL,
    "account_from_id"       INT          NOT NULL,
    "account_to_id"         INT          NOT NULL,
    "transactionDate"       TIMESTAMP    DEFAULT NULL,
    "transactionSum"        FLOAT        DEFAULT NULL,
    "limitExceeded"         BOOLEAN      DEFAULT FALSE,
    "currency_id"           INT          DEFAULT 2,
    "expense_id"            INT          NOT NULL,
    "transaction_limit_id"  INT          DEFAULT NULL,


    CONSTRAINT bank_transactions_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_bank_transaction_currency_category
        FOREIGN KEY (currency_id)
            REFERENCES currency_categories (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_bank_transaction_expense_category
        FOREIGN KEY (expense_id)
            REFERENCES expense_categories (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);


--Модуль курсы валют.

CREATE TABLE currency_rates
(
    "id"          	        SERIAL       NOT NULL,
    "created"     	        TIMESTAMP    NOT NULL,
    "updated"     	        TIMESTAMP    DEFAULT NULL,
    "operationCloseDate"   	TIMESTAMP    DEFAULT NULL,
    "closeSum"              FLOAT        DEFAULT NULL,
    "operationPreviousDate" TIMESTAMP    DEFAULT NULL,
    "previousCloseSum"      FLOAT        DEFAULT NULL,


    CONSTRAINT currency_rates_pkey
        PRIMARY KEY(id)
);







