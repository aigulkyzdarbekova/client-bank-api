package com.solvo.clientbank.account;

import com.solvo.clientbank.Application;
import com.solvo.clientbank.dto.BankTransactionDTO;
import com.solvo.clientbank.models.Account;
import com.solvo.clientbank.models.BankTransaction;
import lombok.Getter;
import lombok.Setter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertNotNull;


@Getter @Setter
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@TestPropertySource("/application-test-sample.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
public class BankTransactionControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${spring.test.root.port}")
    @LocalServerPort
    private int port;

    @Value("${spring.test.root.url}")
    private String url;

    private String getRootUrl(){
        return url + port;
    }


    @Test
    public void createBankTransaction(){

        BankTransactionDTO bankTransactionDTO = new BankTransactionDTO();
        bankTransactionDTO.setAccountFrom(1);
        bankTransactionDTO.setAccountTo(1);
        bankTransactionDTO.setTransactionDate(LocalDateTime.now());
        bankTransactionDTO.setTransactionSum(1000);
        bankTransactionDTO.setCurrencyCategory("KZT");
        bankTransactionDTO.setExpenseCategory("услуги");

        ResponseEntity<BankTransactionDTO> postResponse = restTemplate.postForEntity(getRootUrl() + "/api/transactions", bankTransactionDTO, BankTransactionDTO.class);
        assertNotNull(postResponse);
        assertNotNull(postResponse.getBody());
    }



    @Test
    public void readAllBankTransaction(){

        BankTransactionDTO bankTransactionDTO = new BankTransactionDTO();
        bankTransactionDTO.setAccountFrom(1);
        bankTransactionDTO.setAccountTo(1);
        bankTransactionDTO.setTransactionDate(LocalDateTime.now());
        bankTransactionDTO.setTransactionSum(1000);
        bankTransactionDTO.setCurrencyCategory("USD");
        bankTransactionDTO.setExpenseCategory("услуги");

        ResponseEntity<BankTransactionDTO> postResponse = restTemplate.postForEntity(getRootUrl() + "/api/bank_transactions", bankTransactionDTO, BankTransactionDTO.class);
        assertNotNull(postResponse);
        assertNotNull(postResponse.getBody());
    }



}
