package com.solvo.clientbank.account;

import com.solvo.clientbank.Application;
import com.solvo.clientbank.models.Account;
import lombok.Getter;
import lombok.Setter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.assertNotNull;


@Getter @Setter
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@TestPropertySource("/application-test-sample.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
public class AccountControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${spring.test.root.port}")
    @LocalServerPort
    private int port;

    @Value("${spring.test.root.url}")
    private String url;

    private String getRootUrl(){
        return url + port;
    }


    @Test
    public void createAccount(){
        Account account = new Account();
        account.setNumber(1234);
        ResponseEntity<Account> postResponse = restTemplate.postForEntity(getRootUrl() + "/api/accounts", account, Account.class);
        assertNotNull(postResponse);
        assertNotNull(postResponse.getBody());
    }



}
