package com.example.clientapi.dto;
import lombok.*;


@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class TransactionLimitDTO {

    private double initialLimitAmount;

    private String expenseCategory;
    private String currencyCategory;

    private Integer account;
}
