package com.example.clientapi.services;

import com.example.clientapi.dto.LimitedBankTransactionDTO;
import com.example.clientapi.rest_template.RestTemplateService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LimitedBankTransactionService {
    @Value("${spring.test.root.url}")
    private String url;
    private final RestTemplateService restTemplateService;


    public LimitedBankTransactionService(RestTemplateService restTemplateService) {
        this.restTemplateService = restTemplateService;
    }


    public List<LimitedBankTransactionDTO> read(LimitedBankTransactionDTO dto) {
        return restTemplateService.restTemplatePost(url + "/api/bank_transactions", dto);
    }
}
