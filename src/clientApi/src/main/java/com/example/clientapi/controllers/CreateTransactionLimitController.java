package com.example.clientapi.controllers;


import com.example.clientapi.dto.TransactionLimitDTO;
import com.example.clientapi.services.TransactionLimitService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api-client/transaction-limits")
public class CreateTransactionLimitController {
    private final TransactionLimitService service;

    public CreateTransactionLimitController(TransactionLimitService service) {
        this.service = service;
    }


    @PostMapping
    public TransactionLimitDTO readAll(@RequestBody TransactionLimitDTO transactionLimitDTO) {
        return service.create(transactionLimitDTO);
    }
}
