package com.example.clientapi.dto;
import lombok.*;


@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class AccountDTO {

    private int number;

}
