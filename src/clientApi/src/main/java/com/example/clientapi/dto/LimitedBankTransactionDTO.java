package com.example.clientapi.dto;
import lombok.*;

import java.time.LocalDateTime;


@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class LimitedBankTransactionDTO {


    private Integer accountFrom;

    private Integer accountTo;

    private String currencyCategory;

    private Double transactionSum;

    private String expenseCategory;

    private LocalDateTime transactionDate;


    private Double limitSum;
    private LocalDateTime limitDateTime;
    private String limitCurrencyShortName;

}
