package com.example.clientapi.rest_template;
import com.example.clientapi.dto.LimitedBankTransactionDTO;
import com.example.clientapi.dto.TransactionLimitDTO;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RestTemplateService {
    private final RestTemplate restTemplate;
    HttpHeaders headers = new HttpHeaders();

    public RestTemplateService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<TransactionLimitDTO> restTemplateGet(String url) {
        HttpEntity<TransactionLimitDTO[]> response = restTemplate.getForEntity(url, TransactionLimitDTO[].class);
        TransactionLimitDTO[] transactionLimits = response.getBody();

        return Arrays.stream(transactionLimits)
                .collect(Collectors.toList());
    }

    public void restTemplateDelete(String url) {
        restTemplate.delete(url);
    }

    public List<LimitedBankTransactionDTO> restTemplatePost(String url, Object entity) {
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<?> request = new HttpEntity<>(entity, headers);
        ResponseEntity<LimitedBankTransactionDTO[]> result = restTemplate.postForEntity(url, request, LimitedBankTransactionDTO[].class);
        LimitedBankTransactionDTO[] transactions = result.getBody();

        return Arrays.stream(transactions)
                .collect(Collectors.toList());
    }


    public TransactionLimitDTO restTemplateTransactionLimitPost(String url, Object entity) {
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<?> request = new HttpEntity<>(entity, headers);
        ResponseEntity<TransactionLimitDTO> result = restTemplate.postForEntity(url, request, TransactionLimitDTO.class);
        return  result.getBody();
    }


    public List<TransactionLimitDTO> restTemplateTransactionLimitByAccountPost(String url, Object entity) {
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<?> request = new HttpEntity<>(entity, headers);
        ResponseEntity<TransactionLimitDTO[]> result = restTemplate.postForEntity(url, request, TransactionLimitDTO[].class);
        TransactionLimitDTO[] dtos = result.getBody();

        return Arrays.stream(dtos)
                .collect(Collectors.toList());
    }


}
