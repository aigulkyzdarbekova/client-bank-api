package com.example.clientapi.controllers;


import com.example.clientapi.dto.LimitedBankTransactionDTO;
import com.example.clientapi.services.LimitedBankTransactionService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api-client/limited-transactions")
public class ReadAllLimitedBankTransactionController {
    private final LimitedBankTransactionService service;

    public ReadAllLimitedBankTransactionController(LimitedBankTransactionService service) {
        this.service = service;
    }

    @PostMapping
    public List<LimitedBankTransactionDTO> readAll(@RequestBody LimitedBankTransactionDTO transactionDTO) {
        return (List<LimitedBankTransactionDTO>) service.read(transactionDTO);
    }
}
