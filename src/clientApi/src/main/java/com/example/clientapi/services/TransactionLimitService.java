package com.example.clientapi.services;

import com.example.clientapi.dto.TransactionLimitDTO;
import com.example.clientapi.rest_template.RestTemplateService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;



@Service
public class TransactionLimitService {
    @Value("${spring.test.root.url}")
    private String url;
    private final RestTemplateService restTemplateService;


    public TransactionLimitService(RestTemplateService restTemplateService) {
        this.restTemplateService = restTemplateService;
    }


    public TransactionLimitDTO create(TransactionLimitDTO dto) {
        return restTemplateService.restTemplateTransactionLimitPost(url + "/api/transaction_limits", dto);
    }


}
